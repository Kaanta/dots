#! /bin/sh

# Install packages
sudo pacman -Syu base-devel reflector gcc zsh zsh-completions bash-completion atuin starship eza alacritty mpd ncmpcpp mpv mpc bpytop unrar unzip tar npm bat rustup linux-headers nvidia-dkms bluez bluez-utils openssh

# Change Shell to ZSH
chsh -s /bin/zsh

# Install Rust(Programming Language)
rustup default stable

# Install custom NeoVim configuration if not installed(will change in the future)
if [[ -d "~/.config/nvim" ]]; then
  git clone "https://gitlab.com/kaanta/laniavim" "~/.config/nvim" --depth 1
fi
# Install Paru(Aur Helper written in Rust)
git clone "https://aur.archlinux.org/paru.git";cd paru;makepkg -si;cd ..;sudo rm -r paru 

# Install Window Manager(Hyprland) and other things
paru -Syu neovim-git ttf-iosevka-nerd noto-fonts-cjk hyprland hyprpicker-git less floorp-bin cava swww waybar-git wlroots-git wlr-randr-git bemenu mako-git xdg-desktop-portal-hyprland wl-clip-persist-git qt5-wayland qt5ct libva pamixer cozette-otb pywal-git pavucontrol

ln -s "$HOME/.config/.zshrc" "$HOME/.zshrc"
ln -s "$HOME/.config/.mpd" "$HOME/.mpd"
ln -s "$HOME/.config/Musics" "$HOME/Musics"

zsh
source ~/.zshrc

git config --global user.name "Kaanta"
git config --global user.email "ikaan_oz@hotmail.com"

# sudo nvim /etc/mkinitcpio.conf AND add "nvidia","nvidia_uvm","nvidia_drm","nvidia_modeset"
# sudo nvim /boot/loader/entries/arch.conf AND add "nvidia_drm.modeset=1"
# sudo nvim /etc/modprobe.d/nvidia.conf AND add "options nvidia-drm modeset=1"
# THEN RUN "sudo mkinitcpio --config /etc/mkinitcpio.conf --generate /boot/initramfs-custom.img"

# IF HYPRLAND IS NOT STARTING, RUN THESE :
# git clone https://aur.archlinux.org/hyprland-nvidia.git
# git log --oneline
# git checkout bea42b8
