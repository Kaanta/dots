##| PLUGIN MANAGER |##
[[ -r ~/.config/zsh/znap/znap.zsh ]] ||
    git clone --depth 1 -- \
        https://github.com/marlonrichert/zsh-snap.git ~/.config/zsh/znap
source ~/.config/zsh/znap/znap.zsh  # Start Znap

##| PLUGINS |##
znap source zsh-users/zsh-completions
znap source zsh-users/zsh-autosuggestions
ZSH_AUTOSUGGEST_STRATEGY=(completion)
znap source zdharma-continuum/fast-syntax-highlighting
#znap source marlonrichert/zsh-autocomplete

##| ALIASES |##
# replace 'ls' by 'exa'
alias ls="exa --icons --group-directories-first --color always -lF --git" # File List (with exa)
alias lst="exa --icons --color always -T -l --git" # File List as Tree (with exa)

# get the fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"

# Miscellaneous
alias findit="find | grep"     # Finding specific file and color the desired output
alias grep="grep --color=auto" # Finding specific file and color the desired output
alias kall="killall" # Killall of said processes
alias rm="rm -i" # remove file (ask before doing it)
alias ipa='ip a | grep "inet 192.168."' # grep IP from ip a
alias swimg="swww img --filter=Nearest" #swww wallpaper

##| PROMPT |##
eval "$(starship init zsh)"

##| HISTORY |##
export ATUIN_NOBIND="true"
eval "$(atuin init zsh)" # Launch Atuin
# # depends on terminal mode
bindkey '^[[A' _atuin_search_widget
bindkey '^[OA' _atuin_search_widget


##| PATH |##
if [[ -d "$HOME/.local/bin" ]]; then
  export PATH="$HOME/.local/bin:$PATH"
fi

if [[ -d "$HOME/.cargo/bin" ]]; then
  export PATH="$HOME/.cargo/bin:$PATH"
fi
if [[ -d "$HOME/go/bin" ]]; then
  export PATH="$HOME/go/bin:$PATH"
fi
##| EDITOR |##
export EDITOR="nvim"
export SUDO_EDITOR="nvim" # Set sudo editor as NeoVim
